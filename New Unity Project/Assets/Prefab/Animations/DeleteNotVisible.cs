﻿using UnityEngine;
using System.Collections;

public class DeleteNotVisible : MonoBehaviour {
	void Start(){
		Invoke ("destroy", 3f);
	}

	void destroy(){
		Destroy (this.gameObject);
	}
}
