﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DeleteContent : MonoBehaviour {

	public Button button;
	public Init initClass;
	// Use this for initialization
	void Start () {
		button.onClick.AddListener (deleteContent);
	}

	void deleteContent(){
		LevelControl.control.levelData = new GameData ();
		LevelControl.control.levelData.levels = initClass.makeLevels ();
		PlayerModLoader.loader.playerMods = PlayerModLoader.loader.loadPlayerMods ();
		PlayerModLoader.loader.money = 0;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
