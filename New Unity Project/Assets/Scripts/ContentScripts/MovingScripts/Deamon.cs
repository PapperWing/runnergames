﻿using UnityEngine;
using System.Collections;

public class Deamon : MonoBehaviour {

	public float speed;

	void FixedUpdate(){
		this.transform.position = new Vector2 (this.transform.position.x - Time.fixedDeltaTime * speed, this.transform.position.y);
	}

}
