﻿using UnityEngine;
using System.Collections;
using System;

public class End : MonoBehaviour {

	public int sirkaTlacitka = 200;
	void OnTriggerEnter2D(Collider2D col){
		if (col.tag == "Player") {
			GameTimer.stop = true;
			if (LevelControl.control.levelData.levels [LevelControl.control.actualLevel].BestTime > Math.Round(GameTimer.time ,2)
								|| LevelControl.control.levelData.levels [LevelControl.control.actualLevel].BestTime == 0) {
								LevelControl.control.levelData.levels [LevelControl.control.actualLevel].BestTime = GameTimer.time;
						}
						LevelControl.control.winCond = true;
						if (LevelControl.control.actualLevel < 9) {
								LevelControl.control.levelData.levels [LevelControl.control.actualLevel + 1].unlocked = true;
						}
						LevelControl.control.Save ();
						PlayerModLoader.loader.Save ();
						GameTimer.end = true;
				}
	}
	
}
