﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LevelName : MonoBehaviour {

	public Text text;
	void Start(){
		text.text = LevelControl.control.levelData.levels [LevelControl.control.actualLevel].name;
	}
}
