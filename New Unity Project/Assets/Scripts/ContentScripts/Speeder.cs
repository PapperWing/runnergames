﻿using UnityEngine;
using System.Collections;

public class Speeder : MonoBehaviour {

	public AudioClip speedSound;
	void OnTriggerEnter2D(Collider2D col){
				if (col.gameObject.tag == "Player") {
	
						if (!col.GetComponent<Player> ().zrychlen) {
								col.GetComponent<Player> ().zrychlen = true;
								col.GetComponent<Player> ().Speed += 10;
								AudioSource.PlayClipAtPoint(speedSound, DontDestroy.sound.transform.position,1f);
						}
				}
		}
	void OnTriggerExit2D(Collider2D col){
		if (col.gameObject.tag == "Player") {
						col.GetComponent<Player> ().zrychlen = false;
				}
	}
}
