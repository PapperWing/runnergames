﻿using UnityEngine;
using System.Collections;

public class GameTimer : MonoBehaviour {

	public static float time;
	public static bool stop;
	public static bool end;

	void Awake(){
		time = 0;
		stop = false;
		end = false;
	}	

	void Update () {
		if(!stop && !end)
		time += Time.deltaTime;
	}
}
