﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {



	public AudioClip jumpSound;
	public AudioClip crashSound;

	public float maxSpeed = 10f;
	public float minSpeed = 7f;
	public float addValue = 0.1f;
	public float speed = 0;
	public float jumpForce = 13;
	public int jumps = 0;

	public static bool controlEnabled;
	Animator anim;


	public bool grounded = false;
	public Transform groundCheck;
	private float groundRadius = 0.2f;
	public LayerMask whatIsGround;

	public bool zrychlen = false;

	public static bool isDead;
	public GameObject head;
	public GameObject body;
	public GameObject handleft;
	public GameObject handright;
	

	public float Speed {
		set{ if (speed + value < 12f){
				speed += value;
			}else{
				speed = 12f;
			}
		}
		get{
			return speed;
		}
	}
	void Start () {
		Invoke ("makeSlapota", 0.1f);
		isDead = false;
		added = false;
		lastPos = this.transform.position.x;
		anim = GetComponent<Animator> ();
		speed = 0f;
		jumpForce += LevelControl.control.playerMod.jumpMod / 100;
		minSpeed += LevelControl.control.playerMod.speedMod / 100;
		maxSpeed += LevelControl.control.playerMod.speedMod / 100;
		jumps += LevelControl.control.playerMod.jumps;
	}


	void FixedUpdate () {
		grounded = Physics2D.OverlapCircle (groundCheck.position, groundRadius, whatIsGround);
		if (Physics2D.OverlapCircle (groundCheck.position, groundRadius, whatIsGround)) {
			grounded = true;		
		}
		if (grounded) {
			jumps = LevelControl.control.playerMod.jumps;
		}
		setSpeed ();
		move ();

	}

	public GameObject slapota;
	private bool switche;
	void makeSlapota(){
		if (grounded && !Player.isDead && SpeedManager.speed > 0) {
			var slap = Instantiate(slapota) as GameObject;
			slap.transform.localScale = new Vector3(0.3f,0.3f,1);
			if(switche){
				slap.transform.position = new Vector2(this.transform.position.x - 0.3f, this.transform.position.y- 0.1f);
			}else{
				slap.transform.position = new Vector2(this.transform.position.x - 0.3f, this.transform.position.y- 0f);
			}
			switche = !switche;
		}
		Invoke ("makeSlapota", 0.1f);
	}

	void setSpeed(){
		if (!isDead) {
			if (speed < minSpeed || (speed < maxSpeed && !grounded))
				speed += addValue;
			if (speed > minSpeed && grounded) {
				speed -= addValue;		
			}
		} else {
			if(speed > 0)
				speed -= addValue;
		}
	}
	
	void move(){
		if (!isDead) {
			this.GetComponent<Rigidbody2D>().velocity = new Vector2 (speed, this.GetComponent<Rigidbody2D>().velocity.y);
		} else {
			this.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
		}
	}

	private bool added;
	public float lastPos;
	public GameObject groundJumpPrefab;
	public GameObject airJumpPrefab;
	void Update(){
		anim.SetFloat("Y", this.GetComponent<Rigidbody2D>().velocity.y);
		anim.SetBool ("Grounded", grounded);

		if (this.transform.position.x - lastPos > 0.01) {
			SpeedManager.speed = this.transform.position.x - lastPos;	
		} else {
			SpeedManager.speed = 0;	
		}
		lastPos = this.transform.position.x;
		if (isDead && !added) {
			AudioSource.PlayClipAtPoint(crashSound,DontDestroy.sound.transform.position);
			added = true;
			Destroy(this.GetComponent<BoxCollider2D>());
			Destroy(this.GetComponent<CircleCollider2D>());
			head.AddComponent<Rigidbody2D>().interpolation = RigidbodyInterpolation2D.Interpolate;
			head.GetComponent<Rigidbody2D>().collisionDetectionMode = CollisionDetectionMode2D.Continuous;
			head.AddComponent<CircleCollider2D>().radius= 0.9552326f;
			head.transform.parent = null;
			handright.AddComponent<Rigidbody2D>().interpolation = RigidbodyInterpolation2D.Interpolate;
			handright.GetComponent<Rigidbody2D>().collisionDetectionMode = CollisionDetectionMode2D.Continuous;
			handright.AddComponent<BoxCollider2D>().size = new Vector2(0.8231914f,0.3130007f);
			handright.GetComponent<BoxCollider2D>().offset = new Vector2(-0.3134048f,-0.07288456f);
			handright.transform.parent = null;
			body.AddComponent<Rigidbody2D>().interpolation = RigidbodyInterpolation2D.Interpolate;
			body.GetComponent<Rigidbody2D>().collisionDetectionMode = CollisionDetectionMode2D.Continuous;
			body.AddComponent<BoxCollider2D>();
			body.transform.parent = null;
			handleft.AddComponent<Rigidbody2D>().interpolation = RigidbodyInterpolation2D.Interpolate;
			handleft.GetComponent<Rigidbody2D>().collisionDetectionMode = CollisionDetectionMode2D.Continuous;
			handleft.AddComponent<BoxCollider2D>().size = new Vector2(0.8231914f,0.3130007f);
			handleft.GetComponent<BoxCollider2D>().offset = new Vector2(-0.3134048f,-0.07288456f);
			handleft.transform.parent = null;

			handright.GetComponent<Rigidbody2D>().AddForce(new Vector2(10,0),ForceMode2D.Impulse);
			head.GetComponent<Rigidbody2D>().AddForce(new Vector2(10,0),ForceMode2D.Impulse);
			body.GetComponent<Rigidbody2D>().AddForce(new Vector2(10,0),ForceMode2D.Impulse);
			handleft.GetComponent<Rigidbody2D>().AddForce(new Vector2(10,0),ForceMode2D.Impulse);



		}
		if (!isDead && controlEnabled && !MenuScript.menu) {
#if UNITY_ANDROID
			if ((grounded || jumps > 0) && Input.touchCount > 0 && Input.touches [0].phase == TouchPhase.Began)
#else
		if((grounded || jumps > 0)&& Input.GetKeyDown (KeyCode.Space))
#endif 		
			{
				if(grounded){
					Instantiate(groundJumpPrefab,new Vector2(this.transform.position.x,this.transform.position.y),Quaternion.identity);
				}else{
					Instantiate(airJumpPrefab,new Vector2(this.transform.position.x,this.transform.position.y),Quaternion.identity);
				}
				GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x,jumpForce);
				AudioSource.PlayClipAtPoint(jumpSound,DontDestroy.sound.transform.position);
				jumps--;
			}else{
				anim.SetBool("jump", false);
			}
		}
	}
}