﻿using UnityEngine;
using System.Collections;

public class BGMovement : MonoBehaviour {

	public float speed = 1;

	void FixedUpdate () {
		float newPoint;
		if (SpeedManager.speed > 0 && !GameTimer.end) {
			newPoint = this.transform.position.x - speed * Time.fixedDeltaTime;
		} else {
			newPoint = this.transform.position.x;
		}
		this.transform.position = new Vector2 (newPoint, this.transform.position.y);
	}
}
