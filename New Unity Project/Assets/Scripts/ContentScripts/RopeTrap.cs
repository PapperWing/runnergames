﻿using UnityEngine;
using System.Collections;

public class RopeTrap : MonoBehaviour {

	public float time;
	public IconScript icon;
	public GameObject prefab;
	public DistanceJoint2D joint;
	public GameObject ropeRest;
	public Rigidbody2D ropeBase;


	void Start(){
		var g = Instantiate (prefab) as GameObject;
		g.name = prefab.name;
		g.transform.parent = this.transform;
		joint = g.GetComponent<DistanceJoint2D> ();
		joint.connectedBody = ropeBase;
		g.transform.localPosition = Vector2.zero;

	}

	void OnTriggerEnter2D(Collider2D col){
		if (col.gameObject.tag == "Player") {
			icon.warning();
			Invoke("fall", time);
		}
	}

	void fall(){
		Destroy (joint);
		Destroy (ropeRest);
	}
}
