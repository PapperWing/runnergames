﻿using UnityEngine;
using System.Collections;

public class PlayerClass : MonoBehaviour {

	public SpriteRenderer head;
	public SpriteRenderer body;
	public SpriteRenderer leftHand;
	public SpriteRenderer rightHand;

}
