﻿using UnityEngine;
using System.Collections;

public class PlatformScript : MonoBehaviour {


	void Start(){
		int layerMask = 9;
		if (Physics2D.OverlapArea (new Vector2(this.transform.position.x + this.GetComponent<Collider2D>().bounds.min.x,this.transform.position.y + this.GetComponent<Collider2D>().bounds.min.y), 
		                           new Vector2(this.transform.position.x + this.GetComponent<Collider2D>().bounds.max.x,this.transform.position.y + this.GetComponent<Collider2D>().bounds.max.y), layerMask)) {
			Destroy(this.gameObject);		
		}
	}
}
