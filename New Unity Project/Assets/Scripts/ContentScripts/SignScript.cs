﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SignScript : MonoBehaviour {

	public GameObject prefab;
	public Text text;
	private bool builded;
	void Start(){
		builded = false;
		text.text = string.Format("{0:# ### ### ##0}",((int)(this.transform.position.x/100)* 100)) + "m";
	}

	void OnTriggerEnter2D(Collider2D col){
		if (col.gameObject.tag == "Player" && !this.builded) {
			this.builded = true;
			var g = Instantiate(prefab) as GameObject;
			g.name = "Sign";
			g.transform.position = new Vector2(this.transform.position.x+100,this.transform.position.y);

		}
	}
}
