﻿using UnityEngine;
using System.Collections;

public class Jumper : MonoBehaviour {

	void OnTriggerEnter2D(Collider2D col){
		if (col.gameObject.tag == "Player") {
			col.GetComponent<Player> ().jumpForce += 2;
		}
	}

	void OnTriggerExit2D(Collider2D col){	
		if (col.gameObject.tag == "Player") {
			col.GetComponent<Player> ().jumpForce -= 2;
		}
	}

}
