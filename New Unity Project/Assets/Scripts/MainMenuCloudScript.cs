﻿using UnityEngine;
using System.Collections;

public class MainMenuCloudScript : MonoBehaviour {

	public float speed = 1;
	
	void FixedUpdate () {
		float newPoint;
		newPoint = this.transform.position.x - speed * Time.fixedDeltaTime;
		this.transform.position = new Vector2 (newPoint, this.transform.position.y);
	}
}
