﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MainPageScript : MonoBehaviour {

	public Button mapButton;

	void Start () {
		mapButton.onClick.AddListener (
			() => {
			LevelControl.control.playerMod = PlayerModLoader.loader.playerMods[PlayerModLoader.loader.selectedMod];
			Application.LoadLevel (1);
		});
	}
	

}
