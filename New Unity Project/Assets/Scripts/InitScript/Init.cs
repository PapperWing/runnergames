﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml;

public class Init : MonoBehaviour {

	public GoogleAnalyticsV3 analytics;

	void Start(){
	#if UNITY_ANDROID || UNITY_IPHONE
		analytics.StartSession ();
		AdMob.requestInterstital("ca-app-pub-9038607043217672/5090743540","ca-app-pub-9038607043217672/5090743540");
	#endif
		PlayerPrefs.DeleteAll ();
		Addvisor.counter = 0;
		var g = new GameObject ("LevelControl");
		g.AddComponent<LevelControl> ();
		if (!LevelControl.control.Load ()) {
			var prom = makeLevels();
			LevelControl.control.levelData = new GameData();
			LevelControl.control.levelData.levels = prom;
		}
	}


	public TextAsset inFile;
	public List<Level> makeLevels(){

		var result = new List<Level> ();
		XmlDocument xmlDoc = new XmlDocument();
		xmlDoc.LoadXml(inFile.text);
		XmlNodeList levels = xmlDoc.SelectNodes("//level");
		
		foreach (XmlNode xmlLevel in levels)
		{
			var level = new Level();
			level.id = int.Parse(xmlLevel.Attributes["id"].Value);
			level.name = xmlLevel.Attributes["name"].Value;
			level.unlocked = bool.Parse(xmlLevel.Attributes["unlocked"].Value);
			level.time1 = float.Parse(xmlLevel.Attributes["time1"].Value);
			level.time2 = float.Parse(xmlLevel.Attributes["time2"].Value);
			level.time3 = float.Parse(xmlLevel.Attributes["time3"].Value);
			level.BestTime = 0;
			result.Add(level);
		}

		return result;
	}
	
}
