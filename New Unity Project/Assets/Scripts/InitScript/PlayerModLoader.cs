﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Runtime.Serialization.Formatters.Binary;

public class PlayerModLoader : MonoBehaviour {


	public static PlayerModLoader loader;
	public List<PlayerModifier> playerMods;
	public int selectedMod = 0;
	public int money;
	public int infiniteMeters = 100;
	public float lastRun;
	
	public int Money{
		get{
			return money;
		}set{
			if(money + value > 999999999){
				money = 999999999;
			}else{
				money += value;
			}
		}
	}

	void Awake(){
		if (loader == null) {
			DontDestroyOnLoad (this.gameObject);
			loader = this;
			this.Load();
		} else if (loader != this.gameObject) {
			Destroy(this.gameObject);				
		}
	}

	public TextAsset inFile;
	public List<PlayerModifier> loadPlayerMods(){
		var result = new List<PlayerModifier> ();

		XmlDocument xmlDoc = new XmlDocument();
		xmlDoc.LoadXml(inFile.text);
		XmlNodeList players = xmlDoc.SelectNodes("//player");
		
		foreach (XmlNode xmlPlayer in players)
		{
			var playerMod = new PlayerModifier();
			playerMod.face = xmlPlayer.Attributes["face"].Value;
			playerMod.body = xmlPlayer.Attributes["body"].Value;
			playerMod.hand = xmlPlayer.Attributes["hand"].Value;
			playerMod.jumpMod = float.Parse(xmlPlayer.Attributes["jump"].Value);
			playerMod.speedMod = float.Parse(xmlPlayer.Attributes["speed"].Value);
			playerMod.unlocked = bool.Parse(xmlPlayer.Attributes["unlocked"].Value);
			playerMod.playerName = xmlPlayer.Attributes["name"].Value;
			playerMod.price = int.Parse(xmlPlayer.Attributes["price"].Value);
			playerMod.jumps = int.Parse(xmlPlayer.Attributes["jumps"].Value);

			result.Add(playerMod);
		}

		return result;
	}

	#if UNITY_WEBPLAYER
	public void Save(){
		var saveData = new SaveMod (playerMods,money);
		PlayerPrefs.SetString ("playerMoney", saveData.ToString());
		PlayerPrefs.Save ();
	}
	public bool Load(){
		if (PlayerPrefs.HasKey ("playerMoney")){
			Debug.Log(PlayerPrefs.GetString ("playerMoney"));
			this.playerMods = this.loadPlayerMods();
			var playerMoney = new SaveMod();
			playerMoney.FromString(PlayerPrefs.GetString ("playerMoney"));
			playerMods = new List<PlayerModifier>(playerMoney.merge(playerMods));
			this.money = playerMoney.money;
			this.infiniteMeters = playerMoney.infiniteMeters;
			this.lastRun = playerMoney.lastRun;
			return true;
		}else{
			this.playerMods = this.loadPlayerMods();
			this.money = 0;
			this.infiniteMeters = 100;
			this.lastRun = 0;
			return false;
		}
	}
	
	#else
	public void Save(){
		BinaryFormatter bf = new BinaryFormatter ();
		FileStream file = File.Create (Application.persistentDataPath + "/playerMoney.dat");

		var saveData = new SaveMod (playerMods,money);
		bf.Serialize (file, saveData);
		file.Close ();
	}
	
	public bool Load(){
		if (File.Exists (Application.persistentDataPath + "/playerMoney.dat")) {
			BinaryFormatter bf = new BinaryFormatter ();
			FileStream file = File.Open (Application.persistentDataPath + "/playerMoney.dat", FileMode.Open);
			var data = (SaveMod)bf.Deserialize (file);
			this.playerMods = data.mods;
			money = data.money;
			file.Close ();
			return true;
		} else {
			this.playerMods = this.loadPlayerMods();
			return false;		
		}
	}
	
	#endif
}

[System.Serializable]
public class SaveMod{
	public List<PlayerModifier> mods;
	public int money;
	public int infiniteMeters = 100;
	public float lastRun = 0;

	public SaveMod (){}
	public SaveMod (List<PlayerModifier> pmods, int money){
		this.mods = pmods;
		this.money = money;
	}
	public SaveMod (List<PlayerModifier> pmods, int money, int infiniteMeters){
		this.mods = pmods;
		this.money = money;
		this.infiniteMeters = infiniteMeters;
	}
	public SaveMod (List<PlayerModifier> pmods, int money, int infiniteMeters, float lastRun){
		this.mods = pmods;
		this.money = money;
		this.infiniteMeters = infiniteMeters;
		this.lastRun = lastRun;
	}


	public override string ToString(){
		string result = this.money.ToString() + "-" + this.infiniteMeters.ToString() + "-" + this.lastRun.ToString() + "-";
		foreach (var mod in mods) {
			result += mod.playerName + "," + mod.unlocked.ToString() + "|"	;	
		}
		return result;
	}

	public void FromString(string data){
		mods = new List<PlayerModifier>();
		money = int.Parse(data.Split ('-') [0]);
		infiniteMeters = int.Parse(data.Split ('-') [1]);
		lastRun = float.Parse(data.Split ('-') [2]);
		foreach (var prvek in data.Split ('-')[3].Split('|')) {
			if(prvek != ""){
				PlayerModifier mod = new PlayerModifier();
				mod.playerName = prvek.Split(',')[0];
				mod.unlocked = bool.Parse(prvek.Split (',')[1]);
				mods.Add(mod);
			}
		}
	}

	public List<PlayerModifier> merge(List<PlayerModifier> inMods){
		List<PlayerModifier> result = new List<PlayerModifier> ();
		foreach (PlayerModifier mod in mods) {
			var modified = inMods.Find(x=> x.playerName == mod.playerName);
			modified.unlocked = mod.unlocked;
			result.Add(modified);
		}
		return result;
	}

}