﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BuyButton : MonoBehaviour {

	public GoogleAnalyticsV3 analytics;
	public Button button;
	public Button selectBut;
	public Image selectText;
	public Text buyText;
	public Image selectImage;
	public Image buyImage;
	public Image unlocked;
	public AudioClip cashSound;


	void Start () {
		button.onClick.AddListener (
			()=>{
			if(PlayerModLoader.loader.Money >= PlayerModLoader.loader.playerMods[PlayerModLoader.loader.selectedMod].price){
				PlayerModLoader.loader.playerMods[PlayerModLoader.loader.selectedMod].unlocked = true;
				PlayerModLoader.loader.Money = -PlayerModLoader.loader.playerMods[PlayerModLoader.loader.selectedMod].price;
				LevelControl.control.playerMod = PlayerModLoader.loader.playerMods[PlayerModLoader.loader.selectedMod];
				selectBut.enabled = true;
				button.enabled = false;
				selectText.enabled = true;
				buyText.enabled = false;
				selectImage.enabled = true;
				buyImage.enabled = false;
				unlocked.enabled =false;
				PlayerModLoader.loader.Save();
				AudioSource.PlayClipAtPoint(cashSound,DontDestroy.sound.transform.position);
#if UNITY_ANDROID
				analytics.LogEvent("BuyPlayer","Buy", PlayerModLoader.loader.playerMods[PlayerModLoader.loader.selectedMod].playerName,PlayerModLoader.loader.playerMods[PlayerModLoader.loader.selectedMod].price);
#endif
			}
			});
	}
	void Update (){
		if (PlayerModLoader.loader.Money < PlayerModLoader.loader.playerMods [PlayerModLoader.loader.selectedMod].price) {
			button.interactable = false;
		} else {
			button.interactable = true;
		}
	}

}
