﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MoneyButton : MonoBehaviour {

	public Button button;

	void Start () {
		button.onClick.AddListener (
			()=>{
			PlayerModLoader.loader.Money = 100;
			PlayerModLoader.loader.Save();
			});
	}

}
