﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ContentCreator : MonoBehaviour {

	
	public GameObject playerButton;
	public Transform content;
	public List<GameObject> buttonList;

	public Text playerName;
	public Image headImage;
	public Image bodyImage;
	public Image leftHandImage;
	public Image rightHandImage;
	public Slider jump;
	public Slider speed;
	public Image jumps;
	public Image unlocked;
	public Text unlockedCost;

	public Button selectBut;
	public Button buyBut;
	public Image selectText;
	public Text buyText;
	public Image selectImage;
	public Image buyImage;



	void Start () {

		selectBut.enabled = true;
		buyBut.enabled = false;
		selectText.enabled = true;
		buyText.enabled = false;
		selectImage.enabled = true;
		buyImage.enabled = false;
		PlayerModifier player = PlayerModLoader.loader.playerMods [PlayerModLoader.loader.selectedMod];
		playerName.text = player.playerName;
		headImage.sprite = player.Face;
		bodyImage.sprite = player.Body;
		leftHandImage.sprite = player.Hand;
		rightHandImage.sprite = player.Hand;
		jump.value = player.jumpMod/100;
		speed.value = player.speedMod/100;
		if(player.jumps == 1){jumps.sprite = Resources.Load("jump_double",typeof(Sprite)) as Sprite;}
		if(player.jumps == 2){jumps.sprite = Resources.Load("jump_multi",typeof(Sprite)) as Sprite;}
		if(player.jumps == 3){jumps.sprite = Resources.Load("jump_infinite",typeof(Sprite)) as Sprite;}
		unlocked.enabled =!player.unlocked;
		unlockedCost.enabled  =!player.unlocked;
		unlockedCost.text = string.Format("{0:### ### ##0}",player.price);



		int i = 0;
		foreach (var flayer in PlayerModLoader.loader.playerMods) {
			var g = (GameObject)Instantiate(playerButton);
			g.transform.SetParent(content);
			g.GetComponent<PlayerButtonScript>().pos = i;
			var button = g.GetComponent<Button>();
			g.GetComponent<Image>().sprite = Resources.Load("PlayerScene/effect_podklad",typeof(Sprite)) as Sprite;
			PlayerModifier mod = PlayerModLoader.loader.playerMods [button.GetComponent<PlayerButtonScript>().pos];
			g.GetComponent<PlayerButtonScript>().image.sprite = Resources.Load("PlayerScene/" + mod.playerName, typeof(Sprite)) as Sprite;
			button.onClick.AddListener(()=>{
				buttonList[PlayerModLoader.loader.selectedMod].GetComponent<Image>().color = new Color(0,0,0,0);
				g.GetComponent<Image>().color = Color.white;
				PlayerModLoader.loader.selectedMod = button.GetComponent<PlayerButtonScript>().pos;
				PlayerModifier buttonPlayer = PlayerModLoader.loader.playerMods [button.GetComponent<PlayerButtonScript>().pos];
				playerName.text = buttonPlayer.playerName;
				headImage.sprite = buttonPlayer.Face;
				bodyImage.sprite = buttonPlayer.Body;
				leftHandImage.sprite = buttonPlayer.Hand;
				rightHandImage.sprite = buttonPlayer.Hand;
				jump.value = buttonPlayer.jumpMod/100;
				speed.value = buttonPlayer.speedMod/100;
				if(buttonPlayer.jumps == 1){jumps.sprite = Resources.Load("jump_double",typeof(Sprite)) as Sprite;}
				if(buttonPlayer.jumps == 2){jumps.sprite = Resources.Load("jump_multi",typeof(Sprite)) as Sprite;}
				if(buttonPlayer.jumps == 3){jumps.sprite = Resources.Load("jump_infinite",typeof(Sprite)) as Sprite;}
				unlocked.enabled =!buttonPlayer.unlocked;
				unlockedCost.enabled =!buttonPlayer.unlocked;
				unlockedCost.text = string.Format("{0:### ### ##0}",buttonPlayer.price);
				if (buttonPlayer.unlocked){
					LevelControl.control.playerMod = buttonPlayer;
					selectBut.enabled = true;
					buyBut.enabled = false;
					selectText.enabled = true;
					buyText.enabled = false;
					selectImage.enabled = true;
					buyImage.enabled = false;
				}else{
					buyBut.enabled = true;
					selectBut.enabled = false;
					buyText.enabled = true;
					selectText.enabled = false;
					buyImage.enabled = true;
					selectImage.enabled = false;
				}
			});
			buttonList.Add(g);
			i++;
		}
		buttonList[PlayerModLoader.loader.selectedMod].GetComponent<Image>().color = Color.white;
	}

}
