﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ExitPlayScript : MonoBehaviour {

	public Button play;

	void Start(){
		play.onClick.AddListener (
			() => {
			Application.LoadLevel(1);
		});
	}

}
