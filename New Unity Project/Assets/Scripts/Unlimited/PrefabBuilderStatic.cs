﻿using UnityEngine;
using System.Collections;

public class PrefabBuilderStatic : MonoBehaviour {

	public GameObject prefab;

	public float startTime;
	public float minSpawnTime;
	public float maxSpawnTime;

	void Start () {
	}

	private void spawnPrefab(){
				var g = Instantiate (prefab, this.transform.position, Quaternion.identity) as GameObject;
				g.name = prefab.name;
				if (!paused) {
						var time = Random.Range (minSpawnTime, maxSpawnTime);
						Invoke ("spawnPrefab", time);
				}
		}
	private bool paused;
	void Update(){
		if (Player.isDead){
			Destroy (this.gameObject);
		}
		if (Time.timeScale == 0) {
			paused = true;		
		}
		if (Time.timeScale > 0 && paused) {
			paused = false;
			Invoke("spawnPrefab", Random.Range (minSpawnTime, maxSpawnTime));
		} 

	}

}
