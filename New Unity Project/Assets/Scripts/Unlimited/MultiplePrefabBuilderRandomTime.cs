﻿using UnityEngine;
using System.Collections;

public class MultiplePrefabBuilderRandomTime : MonoBehaviour {
	
	public GameObject[] prefabs;
	
	public float maxSpawnTime;
	public float minSpawnTime;
	
	void Start () {
	}
	
	private void spawnPrefab(){
		var prefab = prefabs [Random.Range (0, prefabs.Length)];
		var g = Instantiate (prefab, this.transform.position, Quaternion.identity) as GameObject;
		g.name = prefab.name;
		if (!paused) {
			var time = Random.Range(minSpawnTime,maxSpawnTime);
			Invoke ("spawnPrefab", time);
		}
	}
	private bool paused;
	void Update(){
		
		if (Player.isDead){
			Destroy (this.gameObject);
		}
		if (Time.timeScale == 0) {
			paused = true;		
		}
		if (Time.timeScale > 0 && paused) {
			paused = false;
			Invoke("spawnPrefab", maxSpawnTime);
		} 
		
	}
}
