﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DistanceScript : MonoBehaviour {


	public Text text;
	public GameObject player;
	public Vector3 startPos;
	public float distance;

	public InfiniteDeadScript deadMenu;
	// Use this for initialization

	
	// Update is called once per frame
	void Update () {
		distance = (player.transform.position.x - startPos.x);
		var reward = (int)(PlayerModLoader.loader.infiniteMeters / 100);
		if (distance < 0) {
			text.text = "0.0 m ";
		}else{
			text.text = string.Format("{0:# ### ### ##0.0}",distance) + " m ";
		}
	}
}
