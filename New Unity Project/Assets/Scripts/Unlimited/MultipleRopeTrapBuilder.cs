﻿using UnityEngine;
using System.Collections;

public class MultipleRopeTrapBuilder : MonoBehaviour {

	public GameObject ropeTrapPrefab;
	public GameObject[] blockPrefabs;
	
	public float startTime;
	public float maxSpawnTime;
	
	void Start () {
	}
	
	private void spawnPrefab(){
				var g = Instantiate (ropeTrapPrefab, this.transform.position, Quaternion.identity) as GameObject;
				g.GetComponent<RopeTrap> ().prefab = blockPrefabs [Random.Range (0, blockPrefabs.Length)];
				g.name = ropeTrapPrefab.name;
				if (!paused) {
						var time = maxSpawnTime;
						Invoke ("spawnPrefab", time);
				}
		}
	private bool paused;
	void Update(){
		if (Player.isDead){
			Destroy (this.gameObject);
		}
		if (Time.timeScale == 0) {
			paused = true;		
		}
		if (Time.timeScale > 0 && paused) {
			paused = false;
			Invoke("spawnPrefab", startTime);
		} 
	}
}
