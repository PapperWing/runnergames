﻿using UnityEngine;
using System.Collections;

public class MultiplePrefabBuilder : MonoBehaviour {

	public GameObject[] prefabs;
	
	public float startTime;
	public float maxSpawnTime;
	
	void Start () {
	}
	
	private void spawnPrefab(){
				var prefab = prefabs [Random.Range (0, prefabs.Length)];
				var g = Instantiate (prefab, this.transform.position, Quaternion.identity) as GameObject;
				g.name = prefab.name;
				if (!paused) {
						var time = maxSpawnTime;
						Invoke ("spawnPrefab", time);
				}
		}
	private bool paused;
	void Update(){

		if (Player.isDead){
			Destroy (this.gameObject);
		}
		if (Time.timeScale == 0) {
			paused = true;		
		}
		if (Time.timeScale > 0 && paused) {
			paused = false;
			Invoke("spawnPrefab", startTime);
		} 

	}
}
