﻿using UnityEngine;
using System.Collections;

public class BuilderMovement : MonoBehaviour {

	public float frequency;
	public float ampl;
	private float center;

	void Start(){
		center = this.transform.position.y;
	}
	void FixedUpdate(){
		this.transform.position = new Vector2(this.transform.position.x,  center + ampl * Mathf.Sin(2*Mathf.PI*frequency*Time.time));
	}
}
