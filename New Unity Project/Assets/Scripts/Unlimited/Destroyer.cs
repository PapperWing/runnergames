﻿using UnityEngine;
using System.Collections;

public class Destroyer : MonoBehaviour {

	void OnTriggerEnter2D(Collider2D col){
		if (col.gameObject.tag != "Player") {
			if (col.gameObject.transform.parent == null) {
				GameObject.Destroy (col.gameObject);
			} else {
				GameObject.Destroy (col.gameObject.transform.parent.gameObject);
			}
		}
	}
}
