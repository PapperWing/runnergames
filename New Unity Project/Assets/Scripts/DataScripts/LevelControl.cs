﻿using UnityEngine;
using System;
using System.Collections;
using System.IO;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;

public class LevelControl : MonoBehaviour {

	public static LevelControl control;

	public GameData levelData;
	
	public int actualLevel;

	public PlayerModifier playerMod;

	public bool winCond;

	void Awake(){
		if (control == null) {
			DontDestroyOnLoad (this.gameObject);
			control = this;
		} else if (control != this.gameObject) {
			Destroy(this.gameObject);				
		}
	}
	
#if UNITY_WEBPLAYER

	public void Save(){
		PlayerPrefs.SetString ("GameData", levelData.ToString());
		PlayerPrefs.Save ();
	}

	public bool Load(){
		if (PlayerPrefs.HasKey ("GameData")){
			levelData = new GameData (PlayerPrefs.GetString ("GameData"));
			return true;
		}else{
			return false;
		}
	}

#else
	public void Save(){
		BinaryFormatter bf = new BinaryFormatter ();
		FileStream file = File.Create (Application.persistentDataPath + "/LevelScript.dat");
		
		bf.Serialize (file, levelData);
		file.Close ();
	}
	
	public bool Load(){
		if (File.Exists (Application.persistentDataPath + "/LevelScript.dat")) {
			BinaryFormatter bf = new BinaryFormatter ();
			FileStream file = File.Open (Application.persistentDataPath + "/LevelScript.dat", FileMode.Open);
			levelData = (GameData)bf.Deserialize (file);
			file.Close ();
			return true;
		} else {
			return false;		
		}
	}

#endif
}

[Serializable]
public class Level{
	public int id;
	public string name;
	public float BestTime = 0;
	public bool unlocked = false;
	public float time1 = 0;
	public float time2 = 0;
	public float time3 = 0;
}

[Serializable]
public class GameData{

	public List<Level> levels;
	

	public GameData(){
		levels = new List<Level> ();
	}
	public GameData(string data){
		levels = new List<Level> ();
		var lData = data.Split('|');
		foreach (var pack in lData) {
			if (pack != "") {
				var level = new Level ();
				var pData = pack.Split (',');
				level.id = int.Parse (pData [0]);
				level.name = pData[1];
				level.BestTime = float.Parse (pData [2]);
				level.unlocked = bool.Parse (pData [4]);
				level.time1 = float.Parse (pData [5]);
				level.time2 = float.Parse (pData [6]);
				level.time3 = float.Parse (pData [7]);
				levels.Add (level);
			}
		}

	}

	public override string ToString(){
		string result = "";
		foreach(var level in levels){
			result += level.id + "," + level.name + "," + level.BestTime + "," + level.unlocked + "," + level.time1 + "," + level.time2 + "," + level.time3 + "|";
		}
		return result;
	}

}
