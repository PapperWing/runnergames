﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Prime31;

public class DeadMenu : MonoBehaviour {


	public Player player;
	public Canvas deadMenuCanvas;
	public DeadMenuContent deadMenuContent;
	public Button pauseButton;

	void Start(){
		reklama = false;
		deadMenuCanvas.enabled = false;
		deadMenuContent.header.text = "You are dead!";
		deadMenuContent.menuButton.onClick.AddListener (
			() => {
				Time.timeScale = 1;
				deadMenuCanvas.enabled = false;
				Application.LoadLevel (1);
				Addvisor.counter ++;
			#if UNITY_ANDROID || UNITY_IPHONE
				AdMob.destroyBanner();
			#endif
			});
		deadMenuContent.resetButton.onClick.AddListener (
			() => {
				Time.timeScale = 1;
				deadMenuCanvas.enabled = false;
				Application.LoadLevel (Application.loadedLevel);
				Addvisor.counter ++;
			#if UNITY_ANDROID || UNITY_IPHONE
				AdMob.destroyBanner();
			#endif
			});
	}
	public bool reklama;
	void Update(){
		if (Player.isDead) {
			if(!reklama){
				reklama = true;
			#if UNITY_ANDROID || UNITY_IPHONE
				AdMob.createBanner("ca-app-pub-9038607043217672/2137277140","ca-app-pub-9038607043217672/2137277140",AdMobBanner.Tablet_728x90,AdMobLocation.TopCenter);
			#endif
			}
			pauseButton.gameObject.SetActive (false);
			deadMenuCanvas.enabled = true;		
		}
	}

}
