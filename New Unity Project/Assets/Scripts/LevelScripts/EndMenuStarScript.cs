﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class EndMenuStarScript : MonoBehaviour {
	
	public Image[] starsImage;
	public Text text;
	public int stars;
	private bool owned;
	// Use this for initialization
	void Update () {
		switch(stars){
		case 1: 
			if(GameTimer.time <= LevelControl.control.levelData.levels[LevelControl.control.actualLevel].time1 && GameTimer.end) owned = true;
			text.text = LevelControl.control.levelData.levels[LevelControl.control.actualLevel].time1.ToString() + "s";
			break;
		case 2: 
			if(GameTimer.time <= LevelControl.control.levelData.levels[LevelControl.control.actualLevel].time2 && GameTimer.end) owned = true;
			text.text = LevelControl.control.levelData.levels[LevelControl.control.actualLevel].time2.ToString() + "s";
			break;
		case 3: 
			if(GameTimer.time <= LevelControl.control.levelData.levels[LevelControl.control.actualLevel].time3 && GameTimer.end) owned = true;
			text.text = LevelControl.control.levelData.levels[LevelControl.control.actualLevel].time3.ToString() + "s";
			break;
		}
		if(owned)
		foreach (Image i in starsImage) {
			i.color = Color.white;		
		}
		
	}
	
	
}