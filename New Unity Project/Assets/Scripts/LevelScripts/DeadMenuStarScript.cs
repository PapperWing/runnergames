﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DeadMenuStarScript : MonoBehaviour {

	public Text text;
	public int stars;
	// Use this for initialization
	void Start () {
		switch(stars){
		case 1: 
			text.text = LevelControl.control.levelData.levels[LevelControl.control.actualLevel].time1.ToString() + "s";
			break;
		case 2: 
			text.text = LevelControl.control.levelData.levels[LevelControl.control.actualLevel].time2.ToString() + "s";
			break;
		case 3: 
			text.text = LevelControl.control.levelData.levels[LevelControl.control.actualLevel].time3.ToString() + "s";
			break;
		}
		
	}
	
	
}