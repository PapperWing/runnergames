﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class EndMenuContent : MonoBehaviour {

	public Text header;
	public Button continueButton;
	public Button menuButton;
	public Button resetButton;
	public Text yourTime;
}
