﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Prime31;

public class MenuScript : MonoBehaviour {


	public static bool menu = false;

	public Canvas pauseMenuCanvas;
	public LevelMenuCanvasContent pauseMenu;
	public Button pauseButton;

	public Player player;

	void Start(){
		menu = false;
		pauseMenuCanvas.enabled = false;

		/*pauseMenu.backButton.onClick.AddListener (
			()=>{
				pauseMenuCanvas.enabled = false;
				Time.timeScale = 1;
				GameTimer.stop = false;
				menu = false;
				pauseButton.gameObject.SetActive (true);
			#if UNITY_ANDROID || UNITY_IPHONE
				AdMob.destroyBanner();
			#endif
			});
		*/
		pauseMenu.menuButton.onClick.AddListener (
			() => {
				Time.timeScale = 1;
				Application.LoadLevel (1);
				Addvisor.counter ++;
			#if UNITY_ANDROID || UNITY_IPHONE
				AdMob.destroyBanner();
			#endif
			});
		pauseMenu.resetButton.onClick.AddListener (
			()=>{
				Time.timeScale = 1;
				Application.LoadLevel(Application.loadedLevel);
				Addvisor.counter ++;
			#if UNITY_ANDROID || UNITY_IPHONE
				AdMob.destroyBanner();
			#endif
			});
		pauseMenu.continueButton.onClick.AddListener (
			()=>{
				pauseMenuCanvas.enabled = false;
				Time.timeScale = 1;
				GameTimer.stop = false;
				menu = false;
				pauseButton.gameObject.SetActive (true);
			#if UNITY_ANDROID || UNITY_IPHONE
				AdMob.destroyBanner();
			#endif
		});

		pauseButton.onClick.AddListener (
			() => {
				if(!GameTimer.end){
					Time.timeScale = 0;
					pauseMenuCanvas.enabled = true;
					menu = true;
					pauseButton.gameObject.SetActive (false);		
				#if UNITY_ANDROID || UNITY_IPHONE
					AdMob.createBanner("ca-app-pub-9038607043217672/2137277140","ca-app-pub-9038607043217672/2137277140",AdMobBanner.Tablet_728x90,AdMobLocation.TopCenter);
				#endif
				}
			});
	}
	
	void Update(){
		if(Input.GetKeyDown(KeyCode.Escape) && !Player.isDead){
			if(menu){
				Time.timeScale = 1;
				pauseMenuCanvas.enabled = false;
				menu = false;
				pauseButton.gameObject.SetActive (true);
			}else{
				Time.timeScale = 0;
				pauseMenuCanvas.enabled = true;
				menu = true;
				pauseButton.gameObject.SetActive (false);
			}
		}
	}

}
