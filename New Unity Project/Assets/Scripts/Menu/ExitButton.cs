﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ExitButton : MonoBehaviour {
	
	public Button button;
	public Image image;
	void Start () {
		button.onClick.AddListener (
			()=>{
			#if UNITY_ANDROID || UNITY_IPHONE
			AdMob.destroyBanner();
			#endif
			Application.LoadLevel(0);
			});
	}
	

}
