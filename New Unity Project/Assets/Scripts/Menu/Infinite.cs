﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Prime31;

public class Infinite : MonoBehaviour {

	public Text infiniteText;
	// Use this for initialization
	void Start () {
		infiniteText.text = "Infinity run \n Best Distance:" + string.Format("{0:# ### ### ##0}",PlayerModLoader.loader.lastRun) + " m";
		this.GetComponent<Button> ().onClick.AddListener (() => {
			#if UNITY_ANDROID || UNITY_IPHONE
			AdMob.destroyBanner();
			#endif
			Application.LoadLevel (13);});
	}

}
