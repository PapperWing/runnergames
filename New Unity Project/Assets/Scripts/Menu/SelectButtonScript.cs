﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SelectButtonScript : MonoBehaviour {

	public Button button;
	public int levelId;
	// Use this for initialization
	void Start () {
		if (LevelControl.control.levelData.levels[levelId-1].unlocked) { 
			button.interactable = true;
		} else {
			button.interactable = false;
		}

		button.onClick.AddListener (levelSwitch);
	}
	
	// Update is called once per frame
	void levelSwitch () {
		#if UNITY_ANDROID || UNITY_IPHONE
		AdMob.destroyBanner();
		#endif
		LevelControl.control.actualLevel = levelId-1;
		Player.isDead = false;
		Application.LoadLevel(levelId+3);
	}
}
