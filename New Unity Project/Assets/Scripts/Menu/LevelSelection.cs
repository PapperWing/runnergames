﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LevelSelection : MonoBehaviour {
	
	public Text levelName;
	public int levelId;
	public float time1;
	public float time2;
	public float time3;
	public Image star1;
	public Image star2;
	public Image star3;

	public void Start(){

		var best = LevelControl.control.levelData.levels [levelId-1].BestTime;
		time1 = LevelControl.control.levelData.levels [levelId-1].time1;
		time2 = LevelControl.control.levelData.levels [levelId-1].time2;
		time3 = LevelControl.control.levelData.levels [levelId-1].time3;

		if (best <= time1 && best > 0) {
			star1.color = Color.white;		
		}
		if (best <= time2 && best > 0) {
			star3.color = Color.white;		
		}
		if (best <= time3 && best > 0) {
			star2.color = Color.white;		
		}
	}
}
