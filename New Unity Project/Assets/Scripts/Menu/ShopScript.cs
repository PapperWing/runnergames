﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Prime31;

public class ShopScript : MonoBehaviour {

	public Image headImage;
	public Image bodyImage;
	public Image leftHandImage;
	public Image rightHandImage;

	public Button button;
	// Use this for initialization
	void Start () {

		#if UNITY_ANDROID || UNITY_IPHONE
		AdMob.destroyBanner();
		AdMob.createBanner("ca-app-pub-9038607043217672/2137277140","ca-app-pub-9038607043217672/2137277140",AdMobBanner.Tablet_468x60,AdMobLocation.TopCenter);
		#endif

		var buttonPlayer = LevelControl.control.playerMod;
		headImage.sprite = buttonPlayer.Face;
		bodyImage.sprite = buttonPlayer.Body;
		leftHandImage.sprite = buttonPlayer.Hand;
		rightHandImage.sprite = buttonPlayer.Hand;
		button.onClick.AddListener (
			()=>{
			#if UNITY_ANDROID || UNITY_IPHONE
			AdMob.destroyBanner();
			#endif
			Application.LoadLevel(2);
			});
	}

}
