﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Statistics : MonoBehaviour {

	public Text tries;
	public Text longestRun;
	
	// Update is called once per frame
	void Update () {
		tries.text = "Tries:" + string.Format("{0:# ### ### ##0}", LevelControl.control.levelData.tries);
		longestRun.text = "Longest Run: " + string.Format("{0:# ### ### ##0.0}",LevelControl.control.levelData.longestRun) + " m ";
	}
}
