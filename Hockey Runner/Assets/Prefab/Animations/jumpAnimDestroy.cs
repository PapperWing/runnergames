﻿using UnityEngine;
using System.Collections;

public class jumpAnimDestroy : MonoBehaviour {

	public float timeInSec;
	void Start(){
		Invoke("DestroyAnim", timeInSec);
	}

	void DestroyAnim(){
		Destroy (this.gameObject);
	}
}
