﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class infoScript : MonoBehaviour {

	public Sprite image1;
	public Sprite image2;
	public Sprite image3;

	public Image vysvetlivkaImage;
	public Button vysvetlivka;
	public Button button;
	// Use this for initialization
	void Start () {
		vysvetlivka.onClick.AddListener (setButtInactive);
		button.onClick.AddListener (setButtActive);
	}
	
	void setButtActive(){
		vysvetlivka.gameObject.SetActive (true);
	}

	void setButtInactive(){
		vysvetlivka.gameObject.SetActive (false);
	}

	void Update(){
		switch (PlayerModLoader.loader.playerMods [PlayerModLoader.loader.selectedMod].jumps) {
		case 1: 
			vysvetlivkaImage.sprite = image1;
			break;
		case 2: 
			vysvetlivkaImage.sprite = image2;
			break;
		case 3: 
			vysvetlivkaImage.sprite = image3;
			break;
		}



	}
}
