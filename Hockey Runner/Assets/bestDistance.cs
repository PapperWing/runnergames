﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class bestDistance : MonoBehaviour {

	public Text text;
	// Use this for initialization
	void Start () {
		text.text = string.Format("{0:# ### ### ##0.0}",LevelControl.control.levelData.longestRun) + "m";
	}
}
