﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class credits : MonoBehaviour {


	public Button credit;
	public Button close;
	public GameObject cred;
	// Use this for initialization
	void Start () {
		credit.onClick.AddListener (openCred);
		close.onClick.AddListener (closeCred);

	}

	void openCred(){
		cred.SetActive (true);
	}
	void closeCred(){
		cred.gameObject.SetActive (false);
	}
}
