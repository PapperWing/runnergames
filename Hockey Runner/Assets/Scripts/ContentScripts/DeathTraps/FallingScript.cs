﻿using UnityEngine;
using System.Collections;

public class FallingScript : MonoBehaviour {


	public Rigidbody2D body;

	void OnTriggerEnter2D( Collider2D col){
		if (col.gameObject.tag == "Player" && body.velocity.y < 0.5) {
			Player.isDead = true;
			GameTimer.stop = true;
			
		}
	}
}
