﻿using UnityEngine;
using System.Collections;

public class DeathTrap : MonoBehaviour {

	public bool invert;
	void Start(){
		if (this.gameObject.tag == "Enemy") {
						this.GetComponent<Animator> ().SetBool ("Invert", invert);
				}
	}
	void OnTriggerEnter2D(Collider2D col){
		if (col.gameObject.tag == "Player" && !GameTimer.end) {
						Player.isDead = true;
						GameTimer.stop = true;
						
				}
	}
}
