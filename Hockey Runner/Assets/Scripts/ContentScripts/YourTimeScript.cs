﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class YourTimeScript : MonoBehaviour {
	
	public Text text;
	void Update () {
		text.text = string.Format("{0:###00}:{1:00}", (int)GameTimer.time % 60, (int)(((float)GameTimer.time % 1.00)*100));
	}
}
