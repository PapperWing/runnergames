﻿using UnityEngine;
using System.Collections;

public class CamMovement : MonoBehaviour {
	
	public GameObject player;
	public float shift = 4f;


	//Kamera sleduje hráče v průběhu celé hry
	void Start () {
		this.transform.position = Vector3.zero;
	}

	void Update () {
		if (!GameTimer.end) {
						this.transform.position = new Vector3 (player.transform.position.x + shift, 0f, -10f);
				}
	}
}
