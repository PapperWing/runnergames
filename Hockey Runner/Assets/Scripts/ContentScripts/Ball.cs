﻿using UnityEngine;
using System.Collections;

public class Ball : MonoBehaviour {

	public AudioClip cashSound;


	void OnTriggerEnter2D(Collider2D col){
		if (col.gameObject.tag == "Player") {
			PlayerModLoader.loader.Money = 1;
			AudioSource.PlayClipAtPoint(cashSound,DontDestroy.sound.transform.position, 0.7f);
			GameObject.Destroy (this.gameObject);
				}
		if(col.gameObject.layer == 9 && col.gameObject.tag != "Circle")

			GameObject.Destroy (this.gameObject);
	}

}
