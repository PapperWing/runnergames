﻿using UnityEngine;
using System.Collections;

public class Shooter : MonoBehaviour {

	public Rigidbody2D obstacle;
	void OnTriggerEnter2D(Collider2D coll) {
		if (coll.gameObject.tag == "Player"){
			obstacle.AddForce(new Vector2(-150,0));		
		}
	}
}
