﻿using UnityEngine;
using System.Collections;

public class Scenar : MonoBehaviour {
	
	public Spawner[] pos;
	void Start(){
		pos [0].delayCall (0f);
		//---------------------
		pos [0].delayCall (2f);
		//---------------------
		pos [0].delayCall (4f);
		pos [1].delayCall (4f);
		//---------------------
		pos [1].delayCall (5f);
		pos [2].delayCall (5f);
		//---------------------
		pos [1].delayCall (6.5f);
		pos [3].delayCall (6.5f);
		pos [4].delayCall (6.5f);
		//---------------------
		pos [0].delayCall (8f);
		pos [3].delayCall (8f);
		pos [4].delayCall (8f);
		//---------------------
		pos [4].delayCall (9.5f);
		pos [1].delayCall (9.5f);
		pos [0].delayCall (9.5f);
		//---------------------
		pos [4].delayCall (11f);
		pos [3].delayCall (11f);
		pos [2].delayCall (11f);
		pos [1].delayCall (11f);
		//---------------------
		pos [4].delayCall (13f);
		pos [3].delayCall (13f);
		pos [0].delayCall (13f);
		//---------------------
		pos [4].delayCall (13.5f);
		pos [3].delayCall (13.7f);
		pos [2].delayCall (14f);
		pos [1].delayCall (14.5f);
		pos [0].delayCall (14.7f);
		//---------------------
		pos [0].delayCall (16f);
		pos [2].delayCall (16.4f);
		pos [3].delayCall (16.7f);
		pos [2].delayCall (17.0f);
		pos [0].delayCall (17.4f);
		//---------------------
		pos [1].delayCall (18f);
		pos [2].delayCall (18f);
		pos [3].delayCall (18f);
		pos [4].delayCall (18f);

		pos [0].delayCall (20.5f);
		pos [3].delayCall (20.5f);
		pos [4].delayCall (20.5f);

		pos [0].delayCall (23f);
		pos [1].delayCall (23f);
		pos [4].delayCall (23f);

		pos [0].delayCall (25f);
		pos [4].delayCall (25f);
	}


	private bool paused;
	void Update(){
		
		if (Player.isDead){
			Destroy (this.gameObject);
		}
		if (Time.timeScale == 0) {
			paused = true;		
		}
		if (Time.timeScale > 0 && paused) {
			paused = false;
		} 
		
	}
}
