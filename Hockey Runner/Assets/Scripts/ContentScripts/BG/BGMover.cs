﻿using UnityEngine;
using System.Collections;

public class BGMover : MonoBehaviour {

	public Transform pos;
	void OnTriggerEnter2D(Collider2D col){
		col.gameObject.transform.position = new Vector2 (pos.position.x ,col.gameObject.transform.position.y);
		if (col.gameObject.tag == "Cloud") {
			Destroy(col.gameObject);		
		}
	}
}
