﻿using UnityEngine;
using System.Collections;

public class Block : MonoBehaviour {

	void OnCollisionEnter2D(Collision2D col){
		if(col.gameObject.tag == "Player")
		col.gameObject.GetComponent<Player> ().speed = col.gameObject.GetComponent<Player> ().speed *0.5f;
	}
}
