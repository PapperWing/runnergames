﻿using UnityEngine;
using System.Collections;

public class IconScript : MonoBehaviour {

	public SpriteRenderer icon;
	public float hideTime= 1.5f;


	public void warning(){
		icon.enabled = true;
		Invoke ("hide", hideTime);
	}

	private void hide(){
		icon.enabled = false;
	}
}
