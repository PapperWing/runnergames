﻿using UnityEngine;
using System.Collections;

public class DontDestroy : MonoBehaviour {

	public static DontDestroy sound;
	public static bool VoiceOn;
	// Use this for initialization
	void Awake(){
		if (sound == null) {
			DontDestroyOnLoad (this.gameObject);
			sound = this;
			VoiceOn = true;
		} else if (sound != this.gameObject) {
			Destroy(this.gameObject);				
		}
	}

	void Update(){
		if (VoiceOn) {
			AudioListener.volume = 0.5f;
		} else {
			AudioListener.volume = 0f;	
		}
	}
}
