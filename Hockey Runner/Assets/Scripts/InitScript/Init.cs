﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml;

public class Init : MonoBehaviour {

	public GoogleAnalyticsV3 analytics;

	void Start(){

	#if UNITY_ANDROID || UNITY_IPHONE
		analytics.StartSession ();
		AdMob.requestInterstital("ca-app-pub-9038607043217672/5090743540","ca-app-pub-9038607043217672/5090743540");
	#endif
		//PlayerPrefs.DeleteAll ();
		Addvisor.counter = 0;
		var g = new GameObject ("LevelControl");
		g.AddComponent<LevelControl> ();
		if (!LevelControl.control.Load ()) {
			LevelControl.control.levelData = new GameData();
		}
	}
}
