﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class PlayerModifier {

	public string id;
	public string face;
	public string body;
	public string hand;
	public float jumpMod;
	public float speedMod;
	public bool unlocked;
	public string playerName;
	public int price;
	public int jumps;

	public Sprite Face{
		get{return (Sprite)(Resources.Load(face,typeof(Sprite)));}
	}
	public Sprite Body{
		get{return (Sprite)(Resources.Load(body,typeof(Sprite)));}
	}
	public Sprite Hand{
		get{return (Sprite)(Resources.Load(hand,typeof(Sprite)));}
	}

}
