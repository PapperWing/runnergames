﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Prime31;

public class EndScript : MonoBehaviour {

	public Canvas endMenuCanvas;
	public EndMenuContent endMenuContent;

	void Start(){
		reklama = false;
		endMenuCanvas.enabled = false;
		endMenuContent.continueButton.onClick.AddListener (
			() => {
				Time.timeScale = 1;
			#if UNITY_ANDROID || UNITY_IPHONE
				AdMob.destroyBanner();
			#endif
				Addvisor.counter ++;	
				if(Application.loadedLevel+1 > 13){
				Application.LoadLevel (1);
				}else{
				LevelControl.control.actualLevel = LevelControl.control.actualLevel+1;
				Application.LoadLevel (Application.loadedLevel+1);
				}
				
			});
		endMenuContent.menuButton.onClick.AddListener (
			() => {
			Time.timeScale = 1;
			#if UNITY_ANDROID || UNITY_IPHONE
			AdMob.destroyBanner();
			#endif
			Addvisor.counter ++;	
			Application.LoadLevel (1);			
		});
		endMenuContent.resetButton.onClick.AddListener (
			()=>{
				Time.timeScale = 1;
				Addvisor.counter ++;
				Application.LoadLevel(Application.loadedLevel);
			#if UNITY_ANDROID || UNITY_IPHONE
				AdMob.destroyBanner();
			#endif
			});
	}
	private bool reklama;
	void Update(){
		if (GameTimer.end) {
			if(!reklama){
				reklama = true;
				#if UNITY_ANDROID || UNITY_IPHONE
				AdMob.createBanner("ca-app-pub-9038607043217672/2137277140","ca-app-pub-9038607043217672/2137277140",AdMobBanner.Tablet_728x90,AdMobLocation.TopCenter);
				#endif
			}
			endMenuContent.header.text = "You have won!";	
			endMenuContent.yourTime.text = "Time: \n " + string.Format("{0:###00}:{1:00}", (int)GameTimer.time % 60, (int)(((float)GameTimer.time % 1.00)*100)) + "s";
			endMenuCanvas.enabled = true;
		}
	}
}
