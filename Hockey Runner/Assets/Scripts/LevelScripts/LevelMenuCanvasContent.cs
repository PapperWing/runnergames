﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LevelMenuCanvasContent : MonoBehaviour {

	public Text header;
	public Button continueButton;
	public Button menuButton;
	public Button resetButton;
}
