﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LevelBuilder : MonoBehaviour {


	public MenuScript menu;
	public DeadMenu deadMenu;
	public GameObject player;

	public CamMovement cam;

	Vector3 position;

	void Start () {
		deadMenu.gameObject.GetComponent<Canvas> ().enabled = false;
		GameTimer.time = 0;
		new GameObject("Timer").AddComponent<GameTimer>();
		if(LevelControl.control != null) 
		LevelControl.control.winCond = false;
		var g = ((GameObject)Instantiate (player));
		g.tag = "Player";
		g.GetComponent<PlayerClass> ().body.sprite = LevelControl.control.playerMod.Body;
		g.GetComponent<PlayerClass> ().leftHand.sprite = LevelControl.control.playerMod.Hand;
		g.GetComponent<PlayerClass> ().rightHand.sprite = LevelControl.control.playerMod.Hand;
		g.GetComponent<PlayerClass> ().head.sprite = LevelControl.control.playerMod.Face;
		g.transform.position = new Vector2(-5.18f,-3f);
		cam.player = g;
		menu.player = g.GetComponent<Player> ();
		deadMenu.player = g.GetComponent<Player> ();
	}
}
