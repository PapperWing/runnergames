﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Prime31;

public class PlayStartButtonMenu : MonoBehaviour {

	public Canvas startCanvas;
	public Button playButton;
	public Button rest;
	public Button pauseButton;
	public Canvas deadMenu;
	public GoogleAnalyticsV3 anal;

	// Use this for initialization
	void Awake () {
		Time.timeScale = 0;
		LevelControl.control.levelData.tries ++;
		LevelControl.control.Save();
		GameTimer.end = false;
		Player.controlEnabled = false;
		GameTimer.time = 0;
		startCanvas.enabled = true;
		playButton.onClick.AddListener (play);
		rest.onClick.AddListener (play);
		pauseButton.gameObject.SetActive (false);
	}

	void Start (){
		#if UNITY_ANDROID || UNITY_IPHONE
		if(Addvisor.counter >= 3 && AdMob.isInterstitalReady()){
			AdMob.displayInterstital();
			AdMob.requestInterstital("ca-app-pub-9038607043217672/2850552343","ca-app-pub-9038607043217672/2850552343");
			Addvisor.counter = 0;
		}
		#endif
		#if UNITY_ANDROID
		anal.LogEvent("SelectLevel","Play", "level" + (Application.loadedLevel-2).ToString(),1);
		#endif
	}

	void play(){
		startCanvas.enabled = false;
		Time.timeScale = 1;
		pauseButton.gameObject.SetActive (true);
		Player.controlEnabled = true;

	}

	void Update(){
		if (startCanvas.enabled) {
			GameTimer.time = 0;	
			deadMenu.enabled = false;
		}
		if (GameTimer.end) {
			pauseButton.gameObject.SetActive (false);				
		}
	}

}
