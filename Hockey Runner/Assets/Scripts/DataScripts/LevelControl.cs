﻿using UnityEngine;
using System;
using System.Collections;
using System.IO;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;

public class LevelControl : MonoBehaviour {

	public static LevelControl control;

	public GameData levelData;
	
	public int actualLevel;

	public PlayerModifier playerMod;

	public bool winCond;

	void Awake(){
		if (control == null) {
			DontDestroyOnLoad (this.gameObject);
			control = this;
		} else if (control != this.gameObject) {
			Destroy(this.gameObject);				
		}
	}
	
#if UNITY_WEBPLAYER

	public void Save(){
		PlayerPrefs.SetString ("GameData", levelData.ToString());
		PlayerPrefs.Save ();
	}

	public bool Load(){
		if (PlayerPrefs.HasKey ("GameData")){
			levelData = new GameData (PlayerPrefs.GetString ("GameData"));
			return true;
		}else{
			return false;
		}
	}

#else
	public void Save(){
		BinaryFormatter bf = new BinaryFormatter ();
		FileStream file = File.Create (Application.persistentDataPath + "/LevelScript.dat");
		
		bf.Serialize (file, levelData);
		file.Close ();
	}
	
	public bool Load(){
		if (File.Exists (Application.persistentDataPath + "/LevelScript.dat")) {
			BinaryFormatter bf = new BinaryFormatter ();
			FileStream file = File.Open (Application.persistentDataPath + "/LevelScript.dat", FileMode.Open);
			levelData = (GameData)bf.Deserialize (file);
			file.Close ();
			return true;
		} else {
			return false;		
		}
	}

#endif
}

[Serializable]
public class GameData{

	public float longestRun;
	public int tries;

	public GameData(){
		longestRun = 0f;
		tries = 0;
	}
	public GameData(string data){
		longestRun = float.Parse(data.Split ('|')[0]);
		tries = int.Parse(data.Split ('|') [1]);
	}

	public override string ToString(){
		return longestRun.ToString() + "|" + tries.ToString();
	}

}
