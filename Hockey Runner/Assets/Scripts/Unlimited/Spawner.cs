﻿using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour {

	public IconScript icon;
	public GameObject prefab;

	public void warning (){
		if (!GameTimer.end && !Player.isDead) {
			icon.warning ();
			Invoke ("spawnDeamon", 2f);
		}
	}

	void spawnDeamon(){
		if (!GameTimer.end && !Player.isDead) {
			Instantiate (prefab, this.transform.position, Quaternion.identity);
		}
	}
	// Update is called once per frame
	void Update () {
	
	}

	public void delayCall(float time){
		Invoke ("warning", time);
	}
}
