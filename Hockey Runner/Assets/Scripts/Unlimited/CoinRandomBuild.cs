﻿using UnityEngine;
using System.Collections;

public class CoinRandomBuild : MonoBehaviour {

	public GameObject[] prefabs;
	// Use this for initialization
	void Start () {
		Instantiate (prefabs [Random.Range (0, prefabs.Length)], this.transform.position, Quaternion.identity);
		Destroy (this.gameObject);
	}
	

}
