﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InfiniteBuilder : MonoBehaviour {
	
	public MenuScript menu;
	public List<GameObject> zones;
	public GameObject player;
	public GameObject startZone;

	public DistanceScript distance;
	public CamMovement cam;
	public InfiniteDeadScript deadMenu;
	
	Vector3 position;
	
	GameObject zoneRoot;

	// Use this for initialization
	void Awake () {
		LevelControl.control.winCond = false;
		zoneRoot = new GameObject ("ZoneRoot");
		startZone.transform.SetParent (zoneRoot.transform);
		new GameObject("Timer").AddComponent<GameTimer>();

		var g = ((GameObject)Instantiate (player));
		g.tag = "Player";
		g.GetComponent<PlayerClass> ().body.sprite = LevelControl.control.playerMod.Body;
		g.GetComponent<PlayerClass> ().leftHand.sprite = LevelControl.control.playerMod.Hand;
		g.GetComponent<PlayerClass> ().rightHand.sprite = LevelControl.control.playerMod.Hand;
		g.GetComponent<PlayerClass> ().head.sprite = LevelControl.control.playerMod.Face;
		g.transform.position = new Vector2(-5.18f,-3f);
		cam.player = g;
		menu.player = g.GetComponent<Player> ();
		deadMenu.player = g.GetComponent<Player> ();
		distance.player = g;
		distance.startPos = new Vector2 (0,0);

		position = new Vector2(14.681f, 0f);
		for (int i = 0; i< 3; i++){
			GameObject zone = ((GameObject)Instantiate (zones[Random.Range(0, zones.Count)]));
			position.x += zone.GetComponent<ZoneScript>().lenghtLeft;
			zone.transform.position = position;
			zone.transform.parent = zoneRoot.transform;
			position.x += zone.GetComponent<ZoneScript>().lenghtRight;
		}

	}
	
	// Update is called once per frame
	void buildLand () {
			GameObject zone = ((GameObject)Instantiate (zones[Random.Range(0, zones.Count)]));
			position.x += zone.GetComponent<ZoneScript>().lenghtLeft;
			zone.transform.position = position;
			zone.transform.parent = zoneRoot.transform;
			position.x += zone.GetComponent<ZoneScript>().lenghtRight;
	}

	void Update(){
		this.transform.position = cam.transform.position;
	}
}
