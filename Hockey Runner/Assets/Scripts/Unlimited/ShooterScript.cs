﻿using UnityEngine;
using System.Collections;

public class ShooterScript : MonoBehaviour {

	public Spawner[] positions;

	public float startTime;
	public float minTime;
	public float maxTime;
	public float lastTime;

	public bool seed;
	void Start(){
		if (seed) {
			Random.seed= LevelControl.control.actualLevel;		
		}
		Invoke ("choosePos", startTime);
	}

	private void choosePos(){
		var pos = positions [Random.Range (0, positions.Length)];
		pos.warning ();
		Invoke ("choosePos", Random.Range (minTime, maxTime));

	}

	private bool paused;
	void Update(){
		
		if (Player.isDead){
			Destroy (this.gameObject);
		}
		if (Time.timeScale == 0) {
			paused = true;		
		}
		if (Time.timeScale > 0 && paused) {
			paused = false;
		} 
		if (lastTime > 0f && GameTimer.time > lastTime) {
			Destroy (this.gameObject);
		}
		
	}
}
