﻿using UnityEngine;
using System.Collections;

public class Invoker : MonoBehaviour {


	// Use this for initialization
	void OnTriggerEnter2D(Collider2D col){
		var builder = GameObject.Find ("Builder");
		if (builder != null) {
			builder.SendMessage("buildLand");		
		}
	}
}
