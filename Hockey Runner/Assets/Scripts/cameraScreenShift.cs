﻿using UnityEngine;
using System.Collections;

public class cameraScreenShift : MonoBehaviour {

	// Use this for initialization
	void Start () {
		var xPos = this.transform.localPosition.x/1.280f * Camera.main.aspect;
		this.gameObject.transform.localPosition = new Vector3 (xPos, this.gameObject.transform.localPosition.y, 10);
	}

}
