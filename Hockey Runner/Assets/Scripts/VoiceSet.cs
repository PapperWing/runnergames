﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class VoiceSet : MonoBehaviour {

	public Button voice;
	public Image voiceImage;
	void Start () {
		voice.onClick.AddListener (change);
	}

	void change(){
		DontDestroy.VoiceOn = !DontDestroy.VoiceOn;
	}

	void Update(){
		if (DontDestroy.VoiceOn) {
			voiceImage.sprite = Resources.Load("buttons/btn_ico_sound1",typeof(Sprite)) as Sprite;
		} else {
			voiceImage.sprite = Resources.Load("buttons/btn_ico_sound3",typeof(Sprite)) as Sprite;	
		}
	}

}
