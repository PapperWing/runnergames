﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Money : MonoBehaviour {

	public Text text;
	// Update is called once per frame
	void Update () {
		text.text = string.Format("{0:### ### ##0}",PlayerModLoader.loader.Money);
	}
}
