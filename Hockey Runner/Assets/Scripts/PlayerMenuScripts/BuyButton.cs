﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BuyButton : MonoBehaviour {

	public GoogleAnalyticsV3 analytics;
	public Button button;
	public Button selectButton;
	public Image unlocked;
	public AudioClip cashSound;


	void Start () {
		button.onClick.AddListener (
			()=>{
			if(PlayerModLoader.loader.Money >= PlayerModLoader.loader.playerMods[PlayerModLoader.loader.selectedMod].price){
				PlayerModLoader.loader.playerMods[PlayerModLoader.loader.selectedMod].unlocked = true;
				PlayerModLoader.loader.Money = -PlayerModLoader.loader.playerMods[PlayerModLoader.loader.selectedMod].price;
				LevelControl.control.playerMod = PlayerModLoader.loader.playerMods[PlayerModLoader.loader.selectedMod];
				unlocked.enabled = false;
				button.gameObject.SetActive(false);
				selectButton.gameObject.SetActive(true);;
				PlayerModLoader.loader.Save();
				AudioSource.PlayClipAtPoint(cashSound,DontDestroy.sound.transform.position);
#if UNITY_ANDROID
				analytics.LogEvent("BuyPlayer","Buy", PlayerModLoader.loader.playerMods[PlayerModLoader.loader.selectedMod].playerName,PlayerModLoader.loader.playerMods[PlayerModLoader.loader.selectedMod].price);
#endif
			}
			});
	}
	void Update (){
		if (PlayerModLoader.loader.Money < PlayerModLoader.loader.playerMods [PlayerModLoader.loader.selectedMod].price) {
			button.interactable = false;
		} else {
			button.interactable = true;
		}
	}

}
