﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ContentCreator : MonoBehaviour {

	

	public Text playerName;
	public Image headImage;
	public Image bodyImage;
	public Image leftHandImage;
	public Image rightHandImage;
	public Slider jump;
	public Slider speed;
	public Image jumps;
	public Image unlocked;
	public Text unlockedCost;

	public Button buyBut;
	public Text buyText;
	public Image buyImage;

	public Button selectedBut;
	public Image selectedButtonImage;
	public Image selectedImage;

	public Button nextButton;
	public Button previousButton;



	void Start () {
		buyBut.gameObject.SetActive(false);
		PlayerModifier player = PlayerModLoader.loader.playerMods [PlayerModLoader.loader.selectedMod];
		playerName.text = player.playerName;
		headImage.sprite = player.Face;
		bodyImage.sprite = player.Body;
		leftHandImage.sprite = player.Hand;
		rightHandImage.sprite = player.Hand;
		jump.value = player.jumpMod/100;
		speed.value = player.speedMod/100;
		if(player.jumps == 1){jumps.sprite = Resources.Load("jump_double",typeof(Sprite)) as Sprite;}
		if(player.jumps == 2){jumps.sprite = Resources.Load("jump_multi",typeof(Sprite)) as Sprite;}
		if(player.jumps == 3){jumps.sprite = Resources.Load("jump_infinite",typeof(Sprite)) as Sprite;}
		unlocked.enabled =!player.unlocked;
		unlockedCost.enabled  =!player.unlocked;
		unlockedCost.text = string.Format("{0:### ### ##0}",player.price);
		if (player.unlocked){
			LevelControl.control.playerMod = player;
			buyBut.gameObject.SetActive(false);
			selectedBut.gameObject.SetActive(true);
			
		}else{
			buyBut.gameObject.SetActive(true);
			selectedBut.gameObject.SetActive(false);
		}


		nextButton.onClick.AddListener(()=>{
			PlayerModLoader.loader.selectedMod = (PlayerModLoader.loader.selectedMod + 1) % PlayerModLoader.loader.playerMods.Count;
			if(PlayerModLoader.loader.selectedMod < 0){
				PlayerModLoader.loader.selectedMod += PlayerModLoader.loader.playerMods.Count;
			}
			PlayerModifier buttonPlayer = PlayerModLoader.loader.playerMods [PlayerModLoader.loader.selectedMod];
			playerName.text = buttonPlayer.playerName;
			headImage.sprite = buttonPlayer.Face;
			bodyImage.sprite = buttonPlayer.Body;
			leftHandImage.sprite = buttonPlayer.Hand;
			rightHandImage.sprite = buttonPlayer.Hand;
			jump.value = buttonPlayer.jumpMod/100;
			speed.value = buttonPlayer.speedMod/100;
			if(buttonPlayer.jumps == 1){jumps.sprite = Resources.Load("jump_double",typeof(Sprite)) as Sprite;}
			if(buttonPlayer.jumps == 2){jumps.sprite = Resources.Load("jump_multi",typeof(Sprite)) as Sprite;}
			if(buttonPlayer.jumps == 3){jumps.sprite = Resources.Load("jump_infinite",typeof(Sprite)) as Sprite;}
			unlocked.enabled =!buttonPlayer.unlocked;
			unlockedCost.enabled =!buttonPlayer.unlocked;
			unlockedCost.text = string.Format("{0:### ### ##0}",buttonPlayer.price);
			if (buttonPlayer.unlocked){
				LevelControl.control.playerMod = buttonPlayer;
				buyBut.gameObject.SetActive(false);
				selectedBut.gameObject.SetActive(true);

			}else{
				buyBut.gameObject.SetActive(true);
				selectedBut.gameObject.SetActive(false);
			}
		});

		previousButton.onClick.AddListener(()=>{
			PlayerModLoader.loader.selectedMod = (PlayerModLoader.loader.selectedMod - 1) % PlayerModLoader.loader.playerMods.Count;
			if(PlayerModLoader.loader.selectedMod < 0){
				PlayerModLoader.loader.selectedMod += PlayerModLoader.loader.playerMods.Count;
			}
			PlayerModifier buttonPlayer = PlayerModLoader.loader.playerMods [PlayerModLoader.loader.selectedMod];
			playerName.text = buttonPlayer.playerName;
			headImage.sprite = buttonPlayer.Face;
			bodyImage.sprite = buttonPlayer.Body;
			leftHandImage.sprite = buttonPlayer.Hand;
			rightHandImage.sprite = buttonPlayer.Hand;
			jump.value = buttonPlayer.jumpMod/100;
			speed.value = buttonPlayer.speedMod/100;
			if(buttonPlayer.jumps == 1){jumps.sprite = Resources.Load("jump_double",typeof(Sprite)) as Sprite;}
			if(buttonPlayer.jumps == 2){jumps.sprite = Resources.Load("jump_multi",typeof(Sprite)) as Sprite;}
			if(buttonPlayer.jumps == 3){jumps.sprite = Resources.Load("jump_infinite",typeof(Sprite)) as Sprite;}
			unlocked.enabled =!buttonPlayer.unlocked;
			unlockedCost.enabled =!buttonPlayer.unlocked;
			unlockedCost.text = string.Format("{0:### ### ##0}",buttonPlayer.price);
			if (buttonPlayer.unlocked){
				LevelControl.control.playerMod = buttonPlayer;
				buyBut.gameObject.SetActive(false);
				selectedBut.gameObject.SetActive(true);
				
			}else{
				buyBut.gameObject.SetActive(true);
				selectedBut.gameObject.SetActive(false);

			}
		});
	}

}
