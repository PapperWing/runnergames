﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Prime31;

public class InfiniteDeadScript : MonoBehaviour {


	public Player player;
	private Vector3 startPos;
	public int streak;

	public Canvas deadMenuCanvas;
	public DeadMenuContent deadMenuContent;
	public Button pauseButton;
	public Image pauseButtonImage;
	public Image pauseButtonImage2;
	public Text distanceText;
	public Text best;
	public Text streakText;
	public GoogleAnalyticsV3 analytics;

	public DistanceScript distance;
	private int startMoney;
	bool check;
	// Use this for initialization
	void Start () {
		check = false;
		reklama = false;
		streak = 1;
		startMoney = PlayerModLoader.loader.money;

		
		deadMenuCanvas.enabled = false;
		deadMenuContent.header.text = "You are dead!";
		deadMenuContent.menuButton.onClick.AddListener (
			() => {
			Time.timeScale = 1;
			Application.LoadLevel (1);
			Addvisor.counter ++;
			#if UNITY_ANDROID || UNITY_IPHONE
			AdMob.destroyBanner();
			#endif
		});
		deadMenuContent.resetButton.onClick.AddListener (
			() => {
			Time.timeScale = 1;
			Application.LoadLevel (Application.loadedLevel);
			Addvisor.counter ++;
			#if UNITY_ANDROID || UNITY_IPHONE
			AdMob.destroyBanner();
			#endif
		});
	}

	public bool reklama;
	// Update is called once per frame
	void Update () {
		if (distance.distance >= PlayerModLoader.loader.infiniteMeters) {
			PlayerModLoader.loader.infiniteMeters += 100;
			getReward();
		}


		if (Player.isDead) {
			if(!reklama){
				reklama = true;
			#if UNITY_ANDROID || UNITY_IPHONE
				AdMob.createBanner("ca-app-pub-9038607043217672/1373819145","ca-app-pub-9038607043217672/1373819145",AdMobBanner.Tablet_728x90,AdMobLocation.TopCenter);
			#endif
			}
			distanceText.text = "Distance: " + string.Format("{0:# ### ### ##0.0}",distance.distance) + "m /" + string.Format("{0:# ### ### ##0.0}", PlayerModLoader.loader.infiniteMeters) + "m";
			best.text = "Best: " + string.Format("{0:# ### ### ##0.0}",LevelControl.control.levelData.longestRun) + "m";
			if(LevelControl.control.levelData.longestRun <= distance.distance){
				LevelControl.control.levelData.longestRun = distance.distance;
#if UNITY_ANDROID
				if(!check){
					check = true;
					analytics.LogEvent("LongestRun",LevelControl.control.playerMod.id.ToString(), LevelControl.control.levelData.longestRun.ToString(),1);
				}
#endif
			}
			streakText.text = (PlayerModLoader.loader.money - startMoney).ToString();
			pauseButtonImage.enabled = false;
			pauseButtonImage2.enabled = false;
			pauseButton.enabled = false;
			deadMenuCanvas.enabled = true;	
			LevelControl.control.Save();
		}

	}

	void getReward(){
		PlayerModLoader.loader.Money = (int)(PlayerModLoader.loader.infiniteMeters/100) * streak;
		streak++;
	}


}
